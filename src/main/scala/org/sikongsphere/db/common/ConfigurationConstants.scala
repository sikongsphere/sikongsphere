package org.sikongsphere.db.common

/**
 * Configuration Constants
 *
 * @author zaiyuan
 * @date 2022/02/27
 * @since 0.1.0
 */
object ConfigurationConstants {

  val GEOMESA_HBASE_CATALOG = "hbase.catalog"
  val GEOMESA_HBASE_ZOOKEEPERS = "hbase.zookeepers"

}
